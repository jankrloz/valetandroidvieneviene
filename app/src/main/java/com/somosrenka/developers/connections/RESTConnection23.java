package com.somosrenka.developers.connections;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;

public class RESTConnection23
{
    //public static final String HOST = "http://192.168.1.72:8000/";
    public static final String HOST = "http://192.168.0.111:8000/";
    private URL url;
    //private String token = "169f319414b63822d7606770a500dd2ec71b136c";
    private HttpURLConnection connection;
    private JSONObject json_ = new JSONObject();
    private StringBuffer Bufferresponse = null;

    //  Constructor de la clase
    public RESTConnection23(String urlString) throws IOException
    {
        this.url = new URL(urlString);
        this.connection = (HttpURLConnection) url.openConnection();
        this.json_ = new JSONObject();
        this.Bufferresponse = new StringBuffer("");
    }

    public void addRequestParam(String key, String value) throws JSONException
        { json_.put(key, value); }

    public void setJsonRequest() throws JSONException
        { this.connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8"); }

    public void setRequestMethod(String method) throws ProtocolException
        { this.connection.setRequestMethod(method); }

    public void addRequestHeader(String key, String value) throws JSONException
        { this.connection.setRequestProperty(key, value); }

    public void addTokenAuthentication(String token) throws JSONException
    { this.connection.setRequestProperty("Authorization", "Token " + token); }


    public void setRequestDoInput(boolean value)
        { connection.setDoInput(value); }

    public void setRequestDoOutput(boolean value)
        { connection.setDoInput(value); }

    public String request(String urlString) {
        StringBuffer chaine = new StringBuffer("");
        try {
            URL url = new URL(urlString);
            String token = "169f319414b63822d7606770a500dd2ec71b136c";
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("User-Agent", "MetallicaHost");
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            connection.setRequestProperty("Authorization", "Token " + token);
            System.out.println(connection.getRequestProperties());
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);

            // USANDO JSON
            JSONObject json_ = new JSONObject();
            JSONObject cred = new JSONObject();
            JSONObject auth = new JSONObject();
            JSONObject parent = new JSONObject();
            cred.put("username", "adm");
            cred.put("password", "pwd");
            auth.put("tenantName", "adm");
            auth.put("passwordCredentials", cred);
            parent.put("auth", auth);

            OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
            wr.write(parent.toString());
            Log.i("RESTConnection23", "La peticion sera : ");
            //OutputStream os = connection.getOutputStream();
            //os.write(parent.toString().getBytes("UTF-8"));
            //Log.i("RESTConnection23", Integer.toString(connection.getResponseCode()));
            //os.close();

            Log.i("RESTConnection23", "YA NO SE QUE PEDO");
            //Log.i("RESTConnection23", os.toString());
            Log.i("RESTConnection23", "");

            //ESCRIBIENDO LOS PARAMETROS A ENVIARSE EN LA PETICION
            Map<String, Object> params = new LinkedHashMap<>();
            params.put("placas", "PlacasDelCarro");
            params.put("color", "RojoPasion");
            params.put("message", "Shark attacks in Botany Bay have gotten out of control. We need more defensive dolphins to protect the schools here, but Mayor Porpoise is too busy stuffing his snout with lobsters. He's so shellfish.");
            StringBuilder postData = new StringBuilder();
            for (Map.Entry<String, Object> param : params.entrySet()) {
                if (postData.length() != 0) postData.append('&');
                postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                postData.append('=');
                postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
            }

            byte[] postDataBytes = postData.toString().getBytes("UTF-8");

            connection.getOutputStream().write(postDataBytes);
            Log.i("RESTConnection23", "La peticion sera : ");
            Log.i("RESTConnection23", connection.getOutputStream().toString());

            //connection.connect();
            Log.i("RESTConnection23", "USANDO EL CONECTION.GETINPUTSTREAM");
            InputStream in = new BufferedInputStream(connection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder result = new StringBuilder();
            String line_;
            while ((line_ = reader.readLine()) != null) {
                result.append(line_);
            }
            Log.i("RESTConnection23", "RESULTADO : ");
            Log.i("RESTConnection23", result.toString());

            Log.i("RESTConnection23", "ya se envio la peticion");
            Log.i("RESTConnection23", connection.toString());
            Log.i("RESTConnection23", "1");
            int responseCode = connection.getResponseCode();
            Log.i("RESTConnection23", connection.getResponseMessage());
            Log.i("RESTConnection23", Integer.toString(responseCode));
            if (responseCode == 200) {
                InputStream inputStream = connection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
                String line = "";
                while ((line = rd.readLine()) != null) {
                    chaine.append(line);
                }
                System.out.println(chaine.toString());
            } else if (responseCode == 401) {
                System.out.println("Autenticate por favor");
                System.out.println(connection.getErrorStream());

                InputStream inputStream = connection.getErrorStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
                String line = "";
                while ((line = rd.readLine()) != null) {
                    chaine.append(line);
                }
                System.out.println("CHAINE401");
                System.out.println(chaine.toString());
            } else if (responseCode == 400) {
                System.out.println("Error 400");
                System.out.println(connection.getErrorStream());

                InputStream inputStream = connection.getErrorStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
                String line = "";
                while ((line = rd.readLine()) != null) {
                    chaine.append(line);
                }
                System.out.println("CHAINE400");
                System.out.println(chaine.toString());
            }

        } catch (IOException e) {
            // writing exception to log
            Log.i("IOExceptoin", e.toString());
        } catch (JSONException e)
        {
            Log.i("JSONExceptoin", e.toString());
        }
        return chaine.toString();
    }

    //metodo general para crear una conexion con el servidor
    public void genericPOSTConnection()
    {
        try
        {
            Log.i("RESTConnection23", "Conternido que se enviara en la peticion ");
            Log.i("RESTConnection23", this.json_.toString());
            OutputStream os = connection.getOutputStream();
            os.write(json_.toString().getBytes("UTF-8"));
            Log.i("RESTConnection23", "Ya se escribieron los parametros del json a la peticion");
            Log.i("RESTConnection23", Integer.toString(connection.getResponseCode()));
            os.close();
            /*
            Log.i("RESTConnection23", "Conternido que se enviara en la peticion ");
            Log.i("RESTConnection23", this.json_.toString());
            OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
            wr.write(json_.toString());
            Log.i("RESTConnection23", "La peticion sera : ");
            Log.i("RESTConnection23", Integer.toString(connection.getResponseCode()));
            //Log.i("RESTConnection23", connection.getOutputStream().toString());
            */
        }
        catch (IOException e) {
            Log.i("RESTConnection23", "Error al intentar enviar la coneccion al servidor");
            Log.i("RESTConnection23", e.toString());
        }

    }

    public void genericGETConnection() throws IOException
    {
        Log.i("ListaAutos","Host: "+HOST);
        Log.i("RESTConnection23", this.json_.toString());
        this.connection.connect();
        Log.i("RESTConnection23", Integer.toString(this.connection.getResponseCode()));

        /*
        BufferedReader rd = new BufferedReader(new InputStreamReader(this.connection.getInputStream()));
        Log.i("RESTConnection23", Integer.toString(this.connection.getResponseCode()));
        String line;
        while ((line = rd.readLine()) != null)
        { this.Bufferresponse.append(line); }
        rd.close();
        */
    }

    //  PETICION PARA REGISTRAR AL USUARIO
    public void RegisterConnection() throws IOException
        { this.genericPOSTConnection(); }

    //Peticion para Login
    public void LoginConnection()throws IOException
        { this.genericPOSTConnection(); }

    //Peticion para Insertar un auto a la base de datos
    public void InsertCarConnection()throws IOException
    {
        Log.i("RESTConnection23", "InsertCarConnection");
        this.genericPOSTConnection(); }


    //Peticion para obtener los autos del usuario
    public void CarListConnection()throws IOException
        { this.genericGETConnection(); }
    public void ServicioConnectio()throws IOException
    { this.genericPOSTConnection(); }
    //Retorna true si la peticion tuvo codigo de respuesta 200 o 201
    public boolean status200or201() throws IOException
    {
        if(this.connection.getResponseCode() == 200 || this.connection.getResponseCode() == 201)
            return true;
        return false;
    }
    //Checa el status de la conexion
    public int getStatus_() throws IOException
    { return this.connection.getResponseCode(); }

    //Retorna el contenido de la respuesta
    public String getResponse() throws IOException
    {
        Log.i("RESTConnection23", "Escribiendo la respuesta en buffer");
        InputStream in = new BufferedInputStream(this.connection.getInputStream());
        BufferedReader rd = new BufferedReader(new InputStreamReader(in));
        String line = "";
        while ((line = rd.readLine()) != null)
            { Bufferresponse.append(line); }
        Log.i("RESTConnection23", "El response fue ");
        Log.i("RESTConnection23", Bufferresponse.toString());





        return Bufferresponse.toString();
    }

    public String getErrorResponse() throws IOException
    {
        //Log.i("RESTConnection23", "");
        Log.i("RESTConnection23", "getErrorResponse ");
        InputStream error_in = new BufferedInputStream(connection.getErrorStream());
        BufferedReader error_rd = new BufferedReader(new InputStreamReader(error_in));
        String line = "";
        while ((line = error_rd.readLine()) != null)
            { Bufferresponse.append(line); }
        //Log.i("RESTConnection23", "getErrorResponse");
        //Log.i("RESTConnection23", Bufferresponse.toString());
        Log.i("RESTConnection23", "El response fue ");
        Log.i("RESTConnection23", Bufferresponse.toString());

        return Bufferresponse.toString();
    }

    public void writeBufferresponse() throws IOException
    {
        Log.i("RESTConnection23", "Escribiendo la respuesta en buffer");
        InputStream in = new BufferedInputStream(this.connection.getInputStream());
        BufferedReader rd = new BufferedReader(new InputStreamReader(in));
        String line = "";
        while ((line = rd.readLine()) != null)
            { Bufferresponse.append(line); }
    }


    public String request_(String urlString)
    {
        StringBuffer chaine = new StringBuffer("");
        try {
            URL url = new URL(urlString);
            String token = "169f319414b63822d7606770a500dd2ec71b136c";
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("User-Agent", "MetallicaHost");
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            connection.setRequestProperty("Authorization", "Token " + token);
            System.out.println(connection.getRequestProperties());
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);

            // USANDO JSON
            JSONObject json_ = new JSONObject();
            json_.put("placas", "misPlacas");
            json_.put("marca", "miMarca");
            json_.put("color", "miColor");

            OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
            wr.write(json_.toString());
            Log.i("RESTConnection23", "La peticion sera : ");
            Log.i("RESTConnection23", Integer.toString(connection.getResponseCode()));
            //Log.i("RESTConnection23", connection.getOutputStream().toString());

            OutputStream os = connection.getOutputStream();
            os.write(json_.toString().getBytes("UTF-8"));
            Log.i("RESTConnection23", Integer.toString(connection.getResponseCode()));
            Log.i("RESTConnection23", "YA NO SE QUE PEDO");
            Log.i("RESTConnection23", os.toString());
            //Log.i("RESTConnection23", os.toString());
            Log.i("RESTConnection23", "");
            os.close();

            //intentando leer convirtiendo la respuesta a json
            // read the Bufferresponse
            int statusCode = connection.getResponseCode();

            if (statusCode == 400) {
                InputStream error_in = new BufferedInputStream(connection.getErrorStream());
                BufferedReader error_rd = new BufferedReader(new InputStreamReader(error_in));
                String line = "";
                while ((line = error_rd.readLine()) != null) {
                    chaine.append(line);
                }
                Log.i("RESTConnection23", "CHAINE400");
                Log.i("RESTConnection23", chaine.toString());

            } else if (statusCode == 200) {
                InputStream in = new BufferedInputStream(connection.getInputStream());
                BufferedReader rd = new BufferedReader(new InputStreamReader(in));
                String line = "";
                while ((line = rd.readLine()) != null) {
                    chaine.append(line);
                }
                Log.i("RESTConnection23", "CHAINE200");
                Log.i("RESTConnection23", chaine.toString());
            } else if (statusCode == 201) {
                InputStream in = new BufferedInputStream(connection.getInputStream());
                BufferedReader rd = new BufferedReader(new InputStreamReader(in));
                String line = "";
                while ((line = rd.readLine()) != null) {
                    chaine.append(line);
                }
                Log.i("RESTConnection23", "CHAINE201");
                Log.i("RESTConnection23", chaine.toString());
            }

        } catch (IOException e) {
            // writing exception to log
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return chaine.toString();
    }


    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public HttpURLConnection getConnection() {
        return connection;
    }

    public void setConnection(HttpURLConnection connection) {
        this.connection = connection;
    }

    public JSONObject getJson_() {
        return json_;
    }

    public void setJson_(JSONObject json_) {
        this.json_ = json_;
    }
}


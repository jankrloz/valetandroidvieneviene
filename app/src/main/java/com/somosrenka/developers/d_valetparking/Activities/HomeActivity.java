package com.somosrenka.developers.d_valetparking.Activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.somosrenka.developers.d_valetparking.R;

public class HomeActivity extends AppCompatActivity
{
    Button Identificarme;
    Button Registrarme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        try {
            Identificarme = (Button) findViewById(R.id.btn_registros);
            Registrarme = (Button) findViewById(R.id.btn_login);


            Identificarme.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                }
            });

            Registrarme.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    Intent viewIntent =
                            new Intent("android.intent.action.VIEW",
                                    Uri.parse("https://www.somosrenka.com/"));
                    startActivity(viewIntent);
                }
            });

        } catch (Exception e) {
            Log.i("HomeActivity", e.toString());
            Log.i("HomeActivity", e.getMessage());
            Log.i("HomeActivity", e.getClass().toString());
        }

    }
}

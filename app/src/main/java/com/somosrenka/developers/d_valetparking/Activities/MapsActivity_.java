package com.somosrenka.developers.d_valetparking.Activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;
import com.somosrenka.developers.Places.PlaceSelectionListeter_;
import com.somosrenka.developers.Places.ValetPlace;
import com.somosrenka.developers.d_valetparking.R;
//import com.somosrenka.developers.Places.GPSTracker;

public class MapsActivity_ extends AppCompatActivity implements OnMapReadyCallback
{
    //      CONSTANTES
    private static final int MY_PERMISSIONS_REQUEST_COARSE_LOCATION = 1;
    private static final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 2;
    private static final String[] PERMISO_FINE = {Manifest.permission.ACCESS_FINE_LOCATION};
    private static final String[] PERMISO_COARSE = {Manifest.permission.ACCESS_COARSE_LOCATION};
    private static final long LOCATION_REFRESH_TIME = 35000;
    private static final float LOCATION_REFRESH_DISTANCE = 2;

    private GoogleMap mMap;
    //private GPSTracker gps;
    private PlaceAutocompleteFragment autocompleteFragment;
    private PlaceSelectionListeter_ placeSelectionHandler;
    private LocationListener locationListener;
    private ValetPlace lugar_destino;
    private SupportMapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_activity_);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_);
        //  AGREGANDO AUTOCOMPLETAR AL MAPA
        autocompleteFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment_);
        autocompleteFragment.getView().setBackgroundColor(Color.RED);
        //CARGAMOS EL MAPA
        mapFragment.getMapAsync(this);

        /*
        //CREANDO UNA PETICION CON VOLLEY PARA PROBAR
        VolleyConnection conn = new VolleyConnection(this);
        conn.makerequest();
        */
        //      NANOZ COMENTARIO
        //  ESTO ES EL RESULTADO DE LA PETICION

        Log.i("MapsActivityCarlos", "TERMINO ONCREATE");
    }

    public void onMapReady(GoogleMap googleMap)
    {
        //Log.i("MapsActivityCarlos", "ONMAPREADY");
        mMap = googleMap;
        // Add a marker in Sydney and move the camera
        LatLng mexico = new LatLng(19.4303674, -99.1373904);
        mMap.addMarker(new MarkerOptions().position(mexico).title("Marcador en df"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(mexico));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mexico, 16)); //mover camara y ponerle un zoom
        //mMap.setOnInfoWindowClickListener(getInfoWindowClickListener());
        mMap.clear();

        //      PEDIMOS PERMISOS
        int permiso_fine_code = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permiso_fine_code != PackageManager.PERMISSION_GRANTED)
        { requestPermission(PERMISO_FINE, MY_PERMISSIONS_REQUEST_FINE_LOCATION);}
        else
        {
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.setMyLocationEnabled(true);
        }
        mMap.setOnInfoWindowClickListener(getInfoWindowClickListener());
        mMap.setOnMarkerClickListener(getMarkerClickListener());
        //Agregando listener de Autocompletar
        //gps = new GPSTracker(this);
        //      CREANDO FILTRO PARA LIMITAR LAS BUSQUEDAS
        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_ESTABLISHMENT).build();
        autocompleteFragment.setFilter(typeFilter);

        placeSelectionHandler = new PlaceSelectionListeter_(mMap);
        autocompleteFragment.setOnPlaceSelectedListener(placeSelectionHandler);

        locationListener = new LocationHandler(this);
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_REFRESH_TIME, LOCATION_REFRESH_DISTANCE, this.locationListener);

        //      DIBUJANDO UN POLIGONO
        //Polygon polygon =
        mMap.addPolygon(new PolygonOptions().add(
                new LatLng(19.439896, -99.18376),
                new LatLng(19.44347, -99.183309),
                new LatLng(19.444695, -99.191645),
                new LatLng(19.442585, -99.19197),
                new LatLng(19.440747, -99.190598)
        ).strokeColor(Color.RED).fillColor(Color.BLUE));
    }

    //METODO PARA OBTENER EL MARCADOR CLICKEADO
    public GoogleMap.OnMarkerClickListener getMarkerClickListener()
    {
        //Log.i("MapsActivityCarlos", "OnMarkerClickListener");
        return new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                //Log.i("MapsActivityCarlos", "@Override onMarkerClick");
                Toast.makeText(getApplicationContext(), "Click en el marcador ..." + marker.getTitle(), Toast.LENGTH_SHORT).show();
                return true;
            }
        };
    }

    public GoogleMap.OnMyLocationButtonClickListener getOnMyLocationButtonClickListener()
    {
        //Log.i("MapsActivityCarlos", "getInfoWindowClickListener");
        return new GoogleMap.OnMyLocationButtonClickListener()
        {
            @Override
            public boolean onMyLocationButtonClick()
            {
                if (mMap.isMyLocationEnabled())
                {Toast.makeText(getApplicationContext(), "MI HUBICACION ACTUAL HABILITADA" , Toast.LENGTH_SHORT).show();
                    return true;
                }
                else
                {Toast.makeText(getApplicationContext(), "HUBICACION ACTUAL DESHABILITADA" , Toast.LENGTH_SHORT).show();}
                return false;
            }
        };

    }

    //METODO PARA MANEJAR CUANDO LE DA CLICK A LA INFORMACION DE UN MARCADOR
    public GoogleMap.OnInfoWindowClickListener getInfoWindowClickListener()
    {
        //Log.i("MapsActivityCarlos", "getInfoWindowClickListener");
        return new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker)
            {Toast.makeText(getApplicationContext(), "Clicked a window with title..." + marker.getTitle(), Toast.LENGTH_SHORT).show();}
        };
    }

    private void requestPermission(String[] _permiso_, int request_code) {
        //ActivityCompat.requestPermissions(mActivity, new String[]{mManifestPersmission}, mRequestCode);
        ActivityCompat.requestPermissions(this, _permiso_, request_code);
    }

    private void promptSettings() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppTheme);
        builder.setTitle("TITULO");
        builder.setMessage("mDeniedNeverAskMsg *MENSAJE PARA PEDIRLE QUE ACTIVE LA UBICACION PARA PODER TRACKEARLO CUANDO LE HA DADO QUE NO QUIERE QUE LE PREGUNTEMOS NUNCA MAS*");
        builder.setPositiveButton("Ir a configuración", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                goToSettings();
                Log.i("MapsActivityCarlos","Termino gotoSettings");
            }
        });
        builder.setNegativeButton("Cancel", null);
        builder.show();
    }

    private void goToSettings() {
        Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + this.getPackageName()));
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
        myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(myAppSettings);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.i("MapsActivityCarlos", "onRequestPermissionsResult");
        switch (requestCode)
        {
            /*
            case MY_PERMISSIONS_REQUEST_COARSE_LOCATION:
            {
                boolean hasSth = grantResults.length > 0;
                if (hasSth) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        //user accepted , make call
                        Log.i("MapsAcptivityCarlos", "permiso concedido");
                        mMap.setMyLocationEnabled(true);

                    }
                    else if(grantResults[0] == PackageManager.PERMISSION_DENIED)
                    {
                        boolean should = ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION);
                        if(should)
                        {
                            //user denied without Never ask again, just show rationale explanation
                            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.Theme_AppInvite_Preview);

                            builder.setTitle("Permiso denegado para COARSE LOCATION");
                            builder.setMessage("La aplicacion necesita del permiso de *COARSE LOCATION* para poder mostrarte los valets.Estas seguro de denegar el permiso?");
                            builder.setPositiveButton("I'M SURE", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which)
                                    {dialog.dismiss(); }
                            });
                            builder.setNegativeButton("RE-TRY", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    dialog.dismiss();
                                    requestPermission(PERMISO_COARSE, MY_PERMISSIONS_REQUEST_COARSE_LOCATION);
                                }
                            });
                            builder.show();
                        }else
                        {
                            //user has denied with `Never Ask Again`, go to settings
                            promptSettings();
                        }
                    }
                }
            }// fin de coarse location
            */
            case MY_PERMISSIONS_REQUEST_FINE_LOCATION:
            {
                boolean hasSth = grantResults.length > 0;
                if(hasSth)
                {
                    if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    {
                        //user accepted , make call
                        Log.i("MapsAcptivityCarlos","permiso concedido");
                        mMap.getUiSettings().setMyLocationButtonEnabled(true);
                        mMap.setMyLocationEnabled(true);
                    }
                    else if(grantResults[0] == PackageManager.PERMISSION_DENIED)
                    {
                        boolean should = ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION);
                        if(should)
                        {
                            //user denied without Never ask again, just show rationale explanation
                            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.Theme_AppInvite_Preview);
                            builder.setTitle("Permiso denegado para FINE LOCATION");
                            builder.setMessage("La aplicacion necesita del permiso de HUBICACION para poder mostrarte los valets.  seguro de denegar el permiso?");
                            builder.setPositiveButton("I'M SURE", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which)
                                {dialog.dismiss();  }
                            });
                            builder.setNegativeButton("RE-TRY", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    dialog.dismiss();
                                    requestPermission(PERMISO_FINE, MY_PERMISSIONS_REQUEST_FINE_LOCATION);
                                }
                            });
                            builder.show();
                        }else
                        {
                            //user has denied with `Never Ask Again`, go to settings
                            promptSettings();
                        }
                    }
                }
            }// fin de request FINE location
        }
    }

    private class LocationHandler implements LocationListener
    {
        private MapsActivity_ locationMapsActivity;
        private Location hubicacion_actual;

        public LocationHandler(MapsActivity_ locationMapsActivity)
            {this.locationMapsActivity = locationMapsActivity;}

        @Override
        public void onLocationChanged(Location locFromGps)
        {
            Log.i("MapaFragment", "OnLocationChanged");
            Log.i("MapaFragment", String.valueOf(locFromGps.getLatitude()));
            Log.i("MapaFragment", String.valueOf(locFromGps.getLongitude()));
            this.setHubicacionActual(locFromGps);
        }

        @Override
        public void onProviderDisabled(String provider)
            {Log.i("onProviderDisabled", "HUBICACION DESACTIVADA");}

        @Override
        public void onProviderEnabled(String provider)
            {Log.i("onProviderEnabled", "HUBICACION ACTIVADA");}

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras)
        {
            Log.i("onStatusChanged", "OnStatusChanged");
            Log.i("onStatusChanged", provider);
            Log.i("onStatusChanged", status+"");
            Log.i("onStatusChanged", extras.toString()+"");
        }

        public void aceptar()
            {Toast.makeText(getApplicationContext(), "Gracias", Toast.LENGTH_SHORT).show();}

        public void setHubicacionActual(Location locFromGps)
            {this.hubicacion_actual = locFromGps;}


    }
}

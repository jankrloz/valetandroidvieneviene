package com.somosrenka.developers.d_valetparking.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.somosrenka.developers.d_valetparking.R;

public class CargandoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cargando);
    }
}

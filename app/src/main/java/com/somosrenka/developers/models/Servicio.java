package com.somosrenka.developers.models;

import java.io.Serializable;

/**
 * Created by thrashforner on 6/07/16.
 */
public class Servicio implements Serializable
{
    private int id;
    private double latitud;
    private double longitud;
    private Valet valet1;


    public Servicio()
    {

    }

    public int getId()
        {return id;}

    public void setId(int id)
        {this.id = id;}

    public double getLatitud()
        {return latitud;}

    public void setLatitud(float latitud)
        {this.latitud = latitud;}

    public double getLongitud()
        {return longitud;}

    public void setLongitud(float longitud)
    {this.longitud = longitud;}

    public Valet getValet1()
        {return valet1;}

    public void setValet1(Valet valet1)
        {this.valet1 = valet1;}
}

package com.somosrenka.developers.models;

import android.app.Application;
import java.io.Serializable;

/**
 * Created by thrashforner on 14/07/16.
 */
public class Usuario implements Serializable
{
    private String token;
    private String email;
    private byte[] password;
    private static Usuario miUsuario;

    protected Usuario(String email, String token,byte[] password)
    {
        this.email=email;
        this.token=token;
        this.password=password;
    }

    protected Usuario()
    {}

    public static void initInstance()
    {
        if (miUsuario== null)
            {miUsuario = new Usuario();}
    }

    public  static Usuario getInstance(String email, String token,byte[] password)
    {
        if (miUsuario==null)
            { miUsuario=new Usuario(email, token, password); }
        return miUsuario;
    }

    public  static Usuario getInstance()
    {
        return miUsuario;
    }


    public static void setInstance(String email, String token,byte[] password)
    { miUsuario = new Usuario(email, token, password); }

    public String getToken()
    { return token; }

    public void setToken(String token)
    { this.token = token; }

    public byte[] getPassword()
    { return password; }

    public void setPassword(byte[] password)
    { this.password = password; }

    public String getEmail()
    { return email; }

    public void setEmail(String email)
    { this.email = email; }

    public String toString()
    { return this.email + " - " + this.token; }

}
// 1f77aed63e95e9edbcb4709e6bec0ca1ba0b7016
package com.somosrenka.developers.models;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

public class Area implements Serializable{
    private String descripcion;
    private LatLng cordenadas;

    public Area() {
    }

    public Area(String descripcion, LatLng cordenadas) {
        this.descripcion = descripcion;
        this.cordenadas=cordenadas;
    }


    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public LatLng getCordenadas() {
        return cordenadas;
    }

    public void setCordenadas(LatLng cordenadas) {
        this.cordenadas = cordenadas;
    }
}

